// node native
import * as path from 'path';

export { path };

// pushrocks scope
import * as smartcli from '@pushrocks/smartcli';
import * as smartfile from '@pushrocks/smartfile';
import * as smartlog from '@pushrocks/smartlog';
import * as smartlogDestinationLocal from '@pushrocks/smartlog-destination-local';
import * as smartpath from '@pushrocks/smartpath';
import * as smartshell from '@pushrocks/smartshell';

export { smartcli, smartfile, smartlog, smartlogDestinationLocal, smartpath, smartshell };

// third party scope
import * as typedoc from 'typedoc';

export { typedoc };
