/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@gitzone/tsdoc',
  version: '1.1.12',
  description: 'a tool for better documentation'
}
